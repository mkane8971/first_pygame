import pygame
from pygame import display, event, image
import game_config as gc
from animal import Animal
from time import sleep


def find_index(x, y):
    row = y // gc.IMAGE_SIZE
    col = x // gc.IMAGE_SIZE
    index = row * gc.NUM_TILES_SIDE + col
    return index


try:
    pygame.init()
except Exception as e:
    print(f"Failed to initialize Pygame: {e}")
    exit(1)
if not pygame.display.get_init():
    print("Failed to initialize the display module.")
    exit(1)
try:
    display.set_caption('Matts Memory Match')
    screen = display.set_mode((512, 512))
except pygame.error as e:
    print(f"Failed to set display Mode: {e}")

matched = image.load(r'other_assets/correct.png')
wrong = image.load(r'other_assets/wrong.png')
win = image.load(r'other_assets/win.png')
screen.blit(matched, (0, 0))
display.flip()

running = True
tiles = [Animal(i) for i in range(0, gc.NUM_TILES_TOTAL)]
current_images = []

while running:
    try:
        current_events = event.get()
        for e in current_events:
            if e.type == pygame.QUIT:
                running = False

            if e.type == pygame.KEYDOWN:
                if e.key == pygame.K_ESCAPE:
                    running = False

            if e.type == pygame.MOUSEBUTTONDOWN:
                mouse_x, mouse_y = pygame.mouse.get_pos()
                index = find_index(mouse_x, mouse_y)
                if index not in current_images:
                    current_images.append(index)
                if len(current_images) > 2:
                    current_images = current_images[1:]

        screen.fill((255, 255, 255))

        total_skipped = 0

        for i, tile in enumerate(tiles):
            image_i = tile.image if i in current_images else tile.box
            if not tile.skip:
                screen.blit(
                    image_i,
                    (tile.col * gc.IMAGE_SIZE + gc.MARGIN,
                     tile.row * gc.IMAGE_SIZE + gc.MARGIN)
                )
            else:
                total_skipped += 1

        display.flip()

        if len(current_images) == 2:
            idx1, idx2 = current_images
            same_name = tiles[idx1].name == tiles[idx2].name
            different_tiles = tiles[idx1] != tiles[idx2]
            if same_name and different_tiles and total_skipped == (
                len(tiles) - 2
            ):
                sleep(0.4)
                screen.blit(win, (0, 0))
                display.flip()
                sleep(2)
                running = False
            elif same_name and different_tiles:
                tiles[idx1].skip = True
                tiles[idx2].skip = True
                sleep(0.3)
                screen.blit(matched, (0, 0))
                display.flip()
                sleep(1)
                current_images = []
            else:
                sleep(0.4)
                screen.blit(wrong, (0, 0))
                display.flip()
                sleep(0.4)
                current_images = []
        display.flip()
    except Exception as e:
        print(f"An error occurred during event processing: {e}")

print('Goodbye!')

import os
import random


IMAGE_SIZE = 128
SCREEN_SIZE = 512
NUM_TILES_SIDE = 4
NUM_TILES_TOTAL = 16
MARGIN = 15

ASSET_DIR = 'assets'
try:
    all_png_files = [
        x for x in os.listdir(ASSET_DIR) if x[-3:].lower() == 'png'
    ]
    ASSET_FILES = random.sample(all_png_files, min(len(all_png_files), 8))
except Exception as e:
    print(f"Failed to list or access directory {ASSET_DIR}: {e}")
    ASSET_FILES = []

assert len(ASSET_FILES) == 8

# First Pygame (Matt's Memory Match)

Matt's Memory Match is a fun, engaging matching game designed to test and improve your memory. In this game, players flip tiles to reveal cute animal images. Match pairs of animals to clear the board and win the game. With a wide array of adorable animal pictures, this game is perfect for players of all ages looking to sharpen their memory skills.

![Animal Match Screenshot](other_assets/Screenshot.png)

## Features

- **Memory Enhancement**: Sharpen your memory by matching pairs of adorable animals.
- **Diverse Animal Images**: Enjoy a variety of animal images that make every game a new adventure.
- **Simple Controls**: Easy-to-learn controls make it accessible for players of all ages.

## Installation

Matt's Memory Match is built with Pygame, so make sure you have Python and Pygame installed on your system. Follow these steps to install and run the game:

# Clone the repository
git clone https://gitlab.com/mkane8971/first_pygame.git

# Navigate to the game directory
cd first_pygame

# Run the game
python app.py

## How to Play

- **Start the Game**: Run the game, and the board filled with covered tiles will appear.
- **Find Pairs**: Click on two tiles to flip them over. If the animals match, they will remain uncovered.
- **Winning**: The game is won when all pairs are matched and uncovered.
- **Restarting**: The game automatically closes after a win so you rerun the application to start again.

## Requirements

- Python 3
- Pygame

Ensure you have the above requirements installed before running the game.

## Contributing

We welcome contributions and suggestions! If you'd like to contribute to Animal Match, please fork the repository and submit a pull request with your proposed changes.

## License

Animal Match is open-source software licensed under the MIT license.

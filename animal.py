import os
import random
import game_config as gc
from pygame import image, transform


animals_count = dict((a, 0) for a in gc.ASSET_FILES)


def available_animals():
    try:
        return [a for a, c in animals_count.items() if c < 2]
    except Exception as e:
        print(f"Could not calculate available animals: {e}")
        return []


class Animal:
    def __init__(self, index):
        self.index = index
        self.row = index // gc.NUM_TILES_SIDE
        self.col = index % gc.NUM_TILES_SIDE
        try:
            self.name = random.choice(available_animals())
            animals_count[self.name] += 1
        except IndexError:
            print("No more available animals to assign")
            self.name = None
        try:
            self.image_path = os.path.join(gc.ASSET_DIR, self.name)
            self.image = image.load(self.image_path)
            self.image = transform.scale(
                self.image,
                (gc.IMAGE_SIZE - 2*gc.MARGIN, gc.IMAGE_SIZE - 2*gc.MARGIN)
            )
        except Exception as e:
            print(f"Failed to load or transform image for {self.name}: {e}")
            self.image = None
        if self.image is None:
            self.box = None
        else:
            self.box = self.image.copy()
        if self.box:
            self.box.fill((200, 200, 200))
        self.skip = False
